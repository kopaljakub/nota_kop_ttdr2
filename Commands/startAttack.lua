function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local originalPosition = {}
local time = 1

local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	local listOfUnits = parameter.listOfUnits 
	self.threshold = 1000

	local teamUnitIds = Spring.GetTeamUnits(Spring.GetMyTeamID())
	local unitsExists = false
	time = time + 1
	
	if not self.running then
		for i=1, #teamUnitIds do
			local unitDefId = Spring.GetUnitDefID(teamUnitIds[i])
			local unitName = UnitDefs[unitDefId].name
			local x, y, z = Spring.GetUnitPosition(teamUnitIds[i]) 
			if z > 4000 and "armham" == unitName then
				local closestEnemie = Spring.GetUnitNearestEnemy(teamUnitIds[i])
				local x1, y1, z1 = Spring.GetUnitPosition(closestEnemie) 
				SpringGiveOrderToUnit(teamUnitIds[i], CMD.ATTACK, {x1, y1, z1}, {"shift"})
				-- SpringGiveOrderToUnit(teamUnitIds[i], CMD.MOVE, {1000, 0, 900}, {"shift"})
			end
		end
	end

	for i=1, #teamUnitIds do
		local unitDefId = Spring.GetUnitDefID(teamUnitIds[i])
		local unitName = UnitDefs[unitDefId].name
		local x, y, z = Spring.GetUnitPosition(teamUnitIds[i]) 
		if z > 4000 and "armham" == unitName then
			unitsExists = true
		end
	end
	

	if unitsExists == true and time < 1000 then
		return RUNNING
	end
	return SUCCESS
end


function Reset(self)
	ClearState(self)
end


