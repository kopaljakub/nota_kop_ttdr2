function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitToTransport",
				variableType = "table",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "target",
				variableType = "table",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local originalPosition = {}
local previousPosition = {}
local currentPosition = {}

local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	self.threshold = 1500
	local radius = 600

	local transporter = parameter.transporter 
	local unitToTransport = parameter.unitToTransport
	local targetPosition = parameter.target

	local target = targetPosition:AsSpringVector()



	if not self.running then
		local x, y, z = Spring.GetUnitPosition(transporter)
		originalPosition = Vec3(x, y, z)
		previousPosition = Vec3(0, 0, 0)


		local sourceX, sourceY, sourceZ = Spring.GetUnitPosition(unitToTransport)
		local origin = originalPosition:AsSpringVector()

		if sourceX < 2000 then
			Spring.GiveOrderToUnit(transporter, CMD.MOVE, {0, 0, 0}, {"shift"})
		end

		Spring.GiveOrderToUnit(transporter, CMD.LOAD_UNITS, {sourceX, sourceY, sourceZ, 10}, {"shift"})

		if sourceX < 2000 then
			Spring.GiveOrderToUnit(transporter, CMD.MOVE, {100, 100, 0}, {"shift"})
		end
		Spring.GiveOrderToUnit(transporter, CMD.UNLOAD_UNITS, {target[1] + math.random(radius) - radius / 2, target[2], target[3] + math.random(radius) - radius / 2, 200}, {"shift"})
		Spring.GiveOrderToUnit(transporter, CMD.MOVE, {origin[1], origin[2], origin[3], 500}, {"shift"})

		local temp = Spring.GetUnitCommands(transporter)
		Spring.Echo(transporter)
		Spring.Echo(#temp)

		self.running = true
	end


	local x = {}
	x[1] = 2
	x[2] = 3
	x[3] = 4
	x[2] = nil
	Spring.Echo(x[2])


	Spring.Echo(transporter)
	local teamUnitIds = Spring.GetTeamUnits(Spring.GetMyTeamID())

	-- for i=1, #teamUnitIds do
	-- 	local unitCommands = Spring.GetUnitCommands(teamUnitIds[i])
	-- 	if #unitCommands > 0 then
	-- 		Spring.Echo(teamUnitIds[i])
	-- 	end
	-- end




	
	-- local pointX, pointY, pointZ = Spring.GetUnitPosition(transporter) 
	-- local currentPosition = Vec3(pointX, pointY, pointZ)
	-- local health, maxHealth, q1, q2, q3 = Spring.GetUnitHealth(transporter)
	-- if (not (previousPosition == currentPosition)) or health ~= maxHealth then
	-- 	previousPosition = currentPosition
	-- 	return RUNNING
	-- end
	-- previousPosition = currentPosition
	-- 	-- if (originalPosition[i]:Distance(currentPosition) > self.threshold) then
	-- 	-- 	return RUNNING
	-- 	-- end
	
	
	
	-- --return SUCCESS
	
	return RUNNING
	
end


function Reset(self)
	ClearState(self)
end
