function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local originalPosition = {}

local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	self.threshold = 1000
	
	return SUCCESS
	
	--return RUNNING
	
end


function Reset(self)
	ClearState(self)
end
