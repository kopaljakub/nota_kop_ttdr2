function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.running=false
end

function Run(self, units, parameter)
	self.threshold = 100

	local listOfUnits = parameter.listOfUnits -- table


	if not self.running then
		for i = 1, #listOfUnits do
			local x = i * ( Game.mapSizeX / #listOfUnits )
			SpringGiveOrderToUnit(listOfUnits[i], CMD.MOVE, {x , 0, 1000, 500 }, {})
		end
		self.running = true
	end

	for i = 1, #listOfUnits do
		local x = i * ( Game.mapSizeX / #listOfUnits )
		SpringGiveOrderToUnit(listOfUnits[i], CMD.MOVE, {x , 0, Game.mapSizeZ - 1000 }, {"shift"})
	end

	

	if #listOfUnits == 0 then
		return SUCCESS
	end
	return RUNNING

	
end


function Reset(self)
	ClearState(self)
end
