function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},			
			{ 
				name = "sources",
				variableType = "table",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local originalPosition = {}
local previousPosition = {}
local currentPosition = {}

local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	self.threshold = 1000


	local listOfUnits = parameter.listOfUnits -- table
	local listOfsources = parameter.sources
	local valley = listOfsources[1]:AsSpringVector()


	if not self.running then
		local max = 0
		local target = Vec3(0, 0, 0)
		for i=1, #listOfsources do
			local source = listOfsources[i]:AsSpringVector()
			if source[1] > max then
				max = source[1]
				target = listOfsources[i]
			end
		end

		local t = target:AsSpringVector()
		for i=1, #listOfUnits do
			SpringGiveOrderToUnit(listOfUnits[i], CMD.MOVE, {t[1], t[2], t[3], 400}, {"shift"})
		end
		self.running = true
	end
	
	for i=1, #listOfUnits do
		local pointX, pointY, pointZ = Spring.GetUnitPosition(listOfUnits[i]) 
		local pointmanPosition = Vec3(pointX, pointY, pointZ)
		if (pointmanPosition:Distance(listOfsources[1]) > self.threshold) then
			return RUNNING
		end
	end
	
	return SUCCESS
	
	-- return RUNNING
	
end


function Reset(self)
	ClearState(self)
end
