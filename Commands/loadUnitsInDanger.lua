function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "sources",
				variableType = "table",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local originalPosition = {}
-- local previousPosition = {}
-- local currentPosition = {}

local function ClearState(self)
	self.running=false
end



function Run(self, units, parameter)
	self.threshold = 200


	local listOfUnits = parameter.listOfUnits -- table
	local listOfsources = parameter.sources


	if not self.running then
		for i=1, #listOfUnits do
			local x, y, z = Spring.GetUnitPosition(listOfUnits[i])
			originalPosition[i] = Vec3(x, y, z)


			
			-- previousPosition[i] = Vec3(0, 0, 0)
		end
		for i = 1, math.min(#listOfsources, #listOfUnits)  do
			local source = listOfsources[i]:AsSpringVector()
			local origin = originalPosition[i]:AsSpringVector()

			SpringGiveOrderToUnit(listOfUnits[i], CMD.LOAD_UNITS, {source[1], source[2], source[3], 10}, {"shift"})
			SpringGiveOrderToUnit(listOfUnits[i], CMD.MOVE, {origin[1], origin[2], origin[3]}, {"shift"})
		end
		self.running = true
	end
	



	for i=1, #listOfUnits do
		-- local pointX, pointY, pointZ = Spring.GetUnitPosition(listOfUnits[i]) 
		-- local currentPosition = Vec3(pointX, pointY, pointZ)
		-- local is_transporting = Spring.GetUnitIsTransporting(listOfUnits[i])
		local health, maxHealth, q1, q2, q3 = Spring.GetUnitHealth(listOfUnits[i])
		if health ~= maxHealth then
			-- previousPosition[i] = currentPosition
			return RUNNING
		end
		-- previousPosition[i] = currentPosition
	end
	
	
	return SUCCESS
	
	
end


function Reset(self)
	ClearState(self)
end
