local sensorInfo = {
	name = "SafeUnits",
	desc = "Return positions of units which can be picked safely.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return units positions
return function()
	local ids = {}
	local index = 1
	local teamUnitIds = Spring.GetTeamUnits(Spring.GetMyTeamID())
	for i=1, #teamUnitIds do
		local used = false
		for j=1, bb.usedIndex do
			if teamUnitIds[i] == bb.used[j] then
				used = true
			end
		end
		local unitDefId = Spring.GetUnitDefID(teamUnitIds[i])
		local x, y, z = Spring.GetUnitPosition(teamUnitIds[i]) 
		if used == false and UnitDefs[unitDefId].canFly and UnitDefs[unitDefId].isTransport then -- (z < 4900 or unitsToRescue == "corrad") and unitsToRescue == unitName then
			local commands = Spring.GetUnitCommands(teamUnitIds[i])
			if #commands == 0 then
				bb.used[bb.usedIndex] = teamUnitIds[i]
				bb.usedIndex = bb.usedIndex + 1
				return teamUnitIds[i]	
			end
		end
	end
end