local sensorInfo = {
	name = "SafeUnits",
	desc = "Return positions of units which can be picked safely.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return units positions
return function(safeArea)
	local center = safeArea.center:AsSpringVector()
	local radius = safeArea.radius

	local ids = {}
	local index = 1

	local teamUnitIds = Spring.GetTeamUnits(Spring.GetMyTeamID())
	for i=1, #teamUnitIds do

		local isTargeted = false
		for j=1, bb.targetedIndex do
			if bb.targeted[j] == teamUnitIds[i] then
				isTargeted = true
			end
		end

		local unitDefId = Spring.GetUnitDefID(teamUnitIds[i])
		local x, y, z = Spring.GetUnitPosition(teamUnitIds[i]) 
		local _, _, _, velocity = Spring.GetUnitVelocity(teamUnitIds[i])
		if UnitDefs[unitDefId].cantBeTransported == false and UnitDefs[unitDefId].canFly == false and z < 4900 and velocity == 0 then
			if x > center[1] - radius and x < center[1] + radius and z > center[3] - radius and z < center[3] + radius then
				-- 
			else
				local emptySlot = false
				for j=1, bb.targetedIndex do
					if bb.targeted[j] == nil then
						bb.targeted[j] = teamUnitIds[i]
						emptySlot = true
					end
				end
				if emptySlot == false then
					bb.targeted[bb.targetedIndex] = teamUnitIds[i]
					bb.targetedIndex = bb.targetedIndex + 1		
				end		
				return teamUnitIds[i]
				-- ids[index] = teamUnitIds[i]
				-- index = index + 1
			end
		end
	end
	return ids
end


-- 	local positionsIndex = 1
-- 	local teamUnitIds = Spring.GetTeamUnits(Spring.GetMyTeamID())
-- 	for i=1, #teamUnitIds do
-- 		local unitDefId = Spring.GetUnitDefID(teamUnitIds[i])
-- 		local unitName = UnitDefs[unitDefId].name
-- 		local x, y, z = Spring.GetUnitPosition(teamUnitIds[i]) 
-- 		if (z < 4900 or unitsToRescue == "corrad") and unitsToRescue == unitName then
-- 			positions[positionsIndex] = Vec3(x, y, z)
-- 			positionsIndex = positionsIndex + 1
-- 		end
-- 	end
-- 	return positions
-- end