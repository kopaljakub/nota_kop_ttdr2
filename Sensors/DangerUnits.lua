local sensorInfo = {
	name = "SafeUnits",
	desc = "Return positions of units which can be picked safely.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return units positions
return function()
	local positions = {}
	local positionsIndex = 1
	local teamUnitIds = Spring.GetTeamUnits(0)
	for i=1, #teamUnitIds do
		local unitDefId = Spring.GetUnitDefID(teamUnitIds[i])
		local unitName = UnitDefs[unitDefId].name
		local x, y, z = Spring.GetUnitPosition(teamUnitIds[i]) 
		-- if z < 6000 and (unitName == "armbox" or unitName == "armbull" or unitName == "armrock" or unitName == "armham") then
		if (z > 6000 and ("armbox" == unitName or "armmllt" == unitName)) then
			positions[positionsIndex] = Vec3(x, y, z)
			positionsIndex = positionsIndex + 1
		end
	end
	return positions
end